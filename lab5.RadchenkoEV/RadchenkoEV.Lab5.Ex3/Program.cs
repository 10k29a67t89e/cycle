﻿using System;

namespace RadchenkoEV.Lab5.Ex3
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int n = Convert.ToInt32(Console.ReadLine());

                while (n > 1)
                {
                    if (n % 2 == 0)
                    {
                        n = n / 2;
                        Console.WriteLine(n);
                    }
                    else
                    {
                        n = ((n * 3) + 1) / 2;
                        Console.WriteLine(n);
                    }
                }
            }
            catch (Exception )
            {
                Console.WriteLine($"Введено не корректное занчение.");
            }
        }
    }
}
