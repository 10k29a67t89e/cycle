﻿using System;

namespace ex1.lab5.RadchenkoEV
{
    class Program
    {
        //для циклов for
        static void Main(string[] args)
        {
            double k, f, x, q, w, e, r, t, y;
            e = -1;
            q = 1;
            w = Math.Sqrt(q * q + e * e) / (q * q + e * e * e);
            r = 1;
            y = -1;
            t = Math.Sqrt(r * r + y * y) / (r * r + y * y * y);
            x = -1;
            k = 1;
            f= Math.Sqrt(k * k + x * x) / (k * k + x * x * x);
            GGG(x, k, f);
            RRR(q, w, e);
            WWW(r, t, y);
        }
        static void GGG(double x, double k, double f)
        { 
            double X = 0.1;
            double K = 1;
            for (x = -1; x <= 1; x = x + X)
            {
                for (k = 1; k <= 10; k = k + K)
                {
                    f = Math.Sqrt(k * k + x * x) / (k * k + x * x * x);
                    Console.Write($"{Math.Round(f, 3)} \t");
                }
                Console.WriteLine();
            }
        }
        //для циклов while
        static void RRR(double q,double w,double e)
        {
            double E = 0.1;
            double Q = 1;
            e = -1;
            while (  e <= 1)
            {
                q = 1;
                while ( q <= 10)
                {
                    w = Math.Sqrt(q * q + e * e) / (q * q + e * e * e);
                    q = q + Q;
                    e = e + E;
                    Console.Write($"{Math.Round(w, 3)} \t");
                }
                Console.WriteLine();
            }
        }
        //для цикла do...while 
        static void WWW(double r, double t, double y)
        {
            double Y = 0.1;
            double R = 1;
            r = 1;
            do
            {
                y = -1;
                do
                {
                    y = y + Y;
                    r = r + R;
                    t = Math.Sqrt(r * r + y * y) / (r * r + y * y * y);
                    Console.Write($"{Math.Round(t, 4)}\t");
                }
                while (y <= 1);
                Console.WriteLine();
            }
            while (r <= 10);
        }
        
    }
}
